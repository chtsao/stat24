+++
title = 'About'
date = 2024-02-15T19:38:18+08:00
draft = false
categories = ["adm"]
+++

![Dice](https://www.simplilearn.com/ice9/free_resources_article_thumb/Data-Science-vs.-Big-Data-vs.jpg)

* Curator: Kno Tsao.  Office: SE A411.  Tel: 3520; [Syllabus](https://sys.ndhu.edu.tw/AA/CLASS/TeacherSubj/prt_tplan.aspx?no=112243846)
* Lectures: Tue. 1310-1500, Thr. 1610-1710 @ AE A210
* [Google Classroom](https://classroom.google.com/c/NjYzOTI1NjM4MTg0?cjc=z33gm4m)
* Office Hours: Mon 14:10-15:00, Thr 15:10-16:00  @ SE A411
* TA Office Hours: 
  * 張秝穎 (四) 09:00~10:00 ＠A412
  * 李錦州 (二) 12:00~13:00  @A408
* Prerequisites: Calculus, Intro to Probability
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)