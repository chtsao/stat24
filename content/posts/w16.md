+++
title = 'Week 16. Finallly.. Final'
date = 2024-05-28T19:42:20+08:00
draft = false

+++

 <img src="https://f4.bcbits.com/img/a0288819336_10.jpg" alt="Running" style="zoom:50%;" />

### Final Exam
* 日期: 2024 0611 (二)
* 時間: 1310-1450.   地點: Virtuality. 線上即時。
* 範圍：本學期上課及習題內容。
* 考古題/範例: 請參考[GoogleClassroom](https://classroom.google.com/c/NjYzOTI1NjM4MTg0?cjc=z33gm4m)公告之2023 年 期中考,期末考 考古題 。請注意之前範圍與形式與本次考試不盡相同。請參考考試範圍。另外，請同學先就筆記/課本/習題內容複習，自己或與同學Quiz 後再試做。比起直接做考古題效果會好很多。
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!



提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！
