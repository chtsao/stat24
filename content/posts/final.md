+++
title = 'Finale'
date = 2024-06-10T09:14:07+08:00
draft = false
+++

![](https://chtsao.gitlab.io/stat24/puffin.faro.jpg)
### 端午快樂！
在 Stat 2024 [GoogleClassroom](https://classroom.google.com/c/NjYzOTI1NjM4MTg0?cjc=z33gm4m) 公佈了 期末考測試上傳。請同學測試熟悉上傳。如有問題，請以 email 與我聯繫。

## Final Exam
* 地點: Virtuality. Open-Book 線上即時。[GoogleClassroom](https://classroom.google.com/c/NjYzOTI1NjM4MTg0?cjc=z33gm4m)。題目將於線上公佈。
* 日期: 2024 0611 (二) 時間: 1310-1450.   
  - **Start**: **0611（二）:1310;**
  - **Deadline: 0611（二）:1455.** (5 min extra for submission)
* Submit your file to our **Google Classroom Classwork**.
* Format: **One** pdf or html file is preferred. Multiple pics are less welcome.

### **規定**
  1. 可討論，可查課本，可參考筆記，可上網，可問人，可 AI。(除了課本/筆記外的參考資料/人/網站/AI 在可知範圍盡量列出，寫在最後報告/答案中)
  2. 自己寫，自己知道，自己知道自己寫什麼，自己寫什麼自己知道
  3. **遠距口試**: 為 釐清回答 以及確定 規定 之遵守度，我們會抽樣部份同學線上口試。請於 **0620 （四）左右**查看學校 gmail 口試通知。

